import React, { Component } from "react";
import {
  Form,
  FormGroup,
  Input,
  TabContent,
  TabPane,
  Nav,
  NavItem,
  NavLink,
  Card,
  Button,
  CardTitle,
  CardText,
  Row,
  Col
} from "reactstrap";
import classnames from "classnames";

export default class AdvancedBookingForm extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      activeTab: "1",
      name: "",
      mobile: "",
      email: "",
      query: ""
    };
    this.submitQueryForm = this.submitQueryForm.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  submitQueryForm(e) {
    e.preventDefault();
    const formObj = {
      name: this.state.name,
      mobile: this.state.mobile,
      email: this.state.email,
      query: this.state.query
    };
    this.props.submitQueryForm(formObj);
  }

  handleChange(e) {
    e.preventDefault();
    let state = this.state;
    state[e.target.name] = e.target.value;
    this.setState({ state });
  }
  toggle(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
    }
  }
  render() {
    return (
      <div className="advanced-booking-Form">
        <Nav tabs className="text-uppercase sub-heading text-center add-space">
          <NavItem>
            <NavLink
              className={classnames({ active: this.state.activeTab === "1" })}
              onClick={() => {
                this.toggle("1");
              }}
            >
              Enquiry Form
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              className={classnames({ active: this.state.activeTab === "2" })}
              onClick={() => {
                this.toggle("2");
              }}
            >
              Booking Form
            </NavLink>
          </NavItem>
        </Nav>
        <TabContent activeTab={this.state.activeTab}>
          <TabPane tabId="1">
            <Row>
              <Col sm="12">
                <Form onSubmit={this.submitQueryForm}>
                  <FormGroup>
                    <Input
                      type="text"
                      name="name"
                      id="name"
                      placeholder="Enter Name"
                      required
                      onChange={this.handleChange}
                    />
                  </FormGroup>
                  <FormGroup>
                    <Input
                      type="email"
                      name="email"
                      id="email"
                      placeholder="Enter Email"
                      required
                      onChange={this.handleChange}
                    />
                  </FormGroup>
                  <FormGroup>
                    <Input
                      type="number"
                      name="mobile"
                      id="phoneNo"
                      placeholder="Enter Contact Number"
                      minLength={10}
                      maxLength={10}
                      required
                      onChange={this.handleChange}
                    />
                  </FormGroup>
                  <FormGroup>
                    <Input
                      rows="2"
                      type="textarea"
                      name="query"
                      id="query"
                      placeholder="Enter your query here"
                      minLength={20}
                      onChange={this.handleChange}
                    />
                  </FormGroup>
                  <div className="text-center">
                    <Button color="success" block type="submit">
                      Submit
                    </Button>
                  </div>
                </Form>
              </Col>
            </Row>
          </TabPane>
          <TabPane tabId="2">
            <Row>
              <Col sm="12">
                <Form>
                  <FormGroup>
                    <Input
                      type="text"
                      name="name"
                      id="name"
                      placeholder="Enter Name"
                    />
                  </FormGroup>
                  <FormGroup>
                    <Input
                      type="email"
                      name="email"
                      id="email"
                      placeholder="Enter Email"
                    />
                  </FormGroup>
                  <FormGroup>
                    <Input
                      type="number"
                      name="phoneNo"
                      id="phoneNo"
                      placeholder="Enter no. of people"
                    />
                  </FormGroup>
                  <FormGroup>
                    <Input
                      rows="2"
                      type="textarea"
                      name="comment"
                      id="comment"
                      placeholder="comments"
                    />
                  </FormGroup>
                  <div className="text-center">
                    <Button color="success" block type="button">
                      Pay now
                    </Button>
                  </div>
                </Form>
              </Col>
            </Row>
          </TabPane>
        </TabContent>
      </div>
    );
  }
}
