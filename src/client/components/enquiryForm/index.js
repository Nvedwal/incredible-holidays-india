import React, { Component } from "react";
import { Form, Row, Col, FormGroup, Label, Input, Button } from "reactstrap";

export default class EnquiryFrom extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      email: "",
      query: ""
    };
    this.submitQueryForm = this.submitQueryForm.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }
  submitQueryForm(e) {
    e.preventDefault();
    const formObj = {
      name: this.state.name,
      email: this.state.email,
      query: this.state.query
    };
    this.props.submitQueryForm(formObj);
  }
  handleChange(e) {
    e.preventDefault();
    const state = this.state;
    state[e.target.name] = e.target.value;
    this.setState({ state });
  }
  render() {
    return (
      <Form onSubmit={this.submitQueryForm}>
        <Row>
          <Col sm="4">
            <FormGroup>
              <Label for="name">Name</Label>
              <Input
                type="text"
                name="name"
                id="name"
                placeholder="Enter Your Name"
                required
                onChange={this.handleChange}
              />
            </FormGroup>
          </Col>
          <Col sm="4">
            <FormGroup>
              <Label for="email">Email</Label>
              <Input
                type="email"
                name="email"
                id="email"
                placeholder="Enter Your Email Id"
                required
                onChange={this.handleChange}
              />
            </FormGroup>
          </Col>
          <Col sm="4">
            <FormGroup>
              <Label for="email">Phone</Label>
              <Input
                type="number"
                name="email"
                id="email"
                placeholder="Enter Your Phone Number"
                required
                onChange={this.handleChange}
              />
            </FormGroup>
          </Col>
          <Col sm="12">
            <FormGroup>
              <Label for="message">Message</Label>
              <Input
                type="textarea"
                name="query"
                id="message"
                placeholder="Leave Your Message Here..."
                required
                minLength={20}
                onChange={this.handleChange}
                rows="4"
              />
            </FormGroup>
          </Col>
          <Col sm="12" className="text-right">
            <Button type="submit" color="success">
              Submit
            </Button>
          </Col>
        </Row>
      </Form>
    );
  }
}
