import React, { Component } from "react";
import { connect } from "react-redux";
import Helmet from "react-helmet";
import { Container, Row, Col } from "reactstrap";
import Loader from "../../components/loader";
import { ToastContainer, toast } from "react-toastify";
import Lightbox from "react-image-lightbox";
import get from "lodash/get";
import config from "../../config/config";
import {
  fetchItinenaryDetails,
  submitItineraryReview
} from "../../redux/ItineraryDetails/action";
import { submitQueryForm } from "../../redux/Global/action";

import MainCarousel from "../../components/carousel";
import ItineraryDayPlan from "../../components/itineraryDayPlan";
import ItineraryDetailsTabs from "../../components/itineraryDetailsTabs";
import ReviewListing from "../../components/reviewLisitng";
import ReviewForm from "../../components/reviewForm";
import AdvancedBookingForm from "../../components/advancedBookingForm";
import SocialShareIcons from "../../components/socialShareIcons";
const _ = { get };

class ItineraryDetails extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isOpen: false,
      imageUrl: ""
    };
    this.onSubmitReview = this.onSubmitReview.bind(this);
    this.submitQueryForm = this.submitQueryForm.bind(this);
  }

  componentDidMount() {
    if (this.props.itineraryDetails !== null) {
      if (this.props.itineraryDetails.slug != this.props.match.params.slug) {
        this.props.fetchItinenaryDetails(this.props.match.params.slug);
      }
    }
    if (this.props.itineraryDetails === null) {
      this.props.fetchItinenaryDetails(this.props.match.params.slug);
    }
    $(window).scrollTop(0);
  }

  componentWillReceiveProps(nextProps) {
    if (
      nextProps.itineraryReviewStatus !== null &&
      nextProps.itineraryReviewStatus !== this.props.itineraryReviewStatus
    ) {
      this.toastSuccess(nextProps.itineraryReviewStatus.message);
    }
  }

  submitQueryForm(data) {
    const formData = {
      itinerary: this.props.itineraryDetails._id,
      ...data
    };
    this.props.submitQueryForm(formData);
  }
  onSubmitReview(data) {
    const ReviewObj = {
      itineraryId: this.props.itineraryDetails._id,
      title: this.props.itineraryDetails.pageTitle,
      url: this.props.history.location.pathname,
      ...data
    };
    this.props.submitItineraryReview(ReviewObj);
  }
  openLIghtBox(url) {
    this.setState({ isOpen: true, imageUrl: url });
  }

  toastSuccess(message) {
    toast.success(message, {
      position: "bottom-center",
      autoClose: 2000,
      hideProgressBar: true,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      draggablePercent: 20
    });
  }

  render() {
    const baseUrl = `${config.serverUrl}:${config.port}`;
    return (
      <div>
        {this.props.itineraryDetails !== null ? (
          <Helmet>
            <title>{`${
              this.props.itineraryDetails.metaTitle
            } - Incredible Holiday India`}</title>
            <meta
              name="description"
              content={this.props.itineraryDetails.metaDescription}
            />
            <meta
              name="keywords"
              content={this.props.itineraryDetails.metaKeywords}
            />
            <meta name="author" content="Holiday Packages In India" />
            <meta name="robots" content="noindex" />
            />
          </Helmet>
        ) : null}
        <div className="carousel-search relative">
          <MainCarousel
            image={
              this.props.itineraryDetails !== null
                ? `${baseUrl}${this.props.itineraryDetails.featuredImage}`
                : "/images/itinerarylisting.jpg"
            }
            name={_.get(this.props.itineraryDetails, "pageTitle", "")}
          />
        </div>
        <section className="section-row">
          {this.props.loadingItineraryDetails === true ||
          this.props.itineraryDetails === null ? (
            <div className="col-sm-12  d-flex justify-content-center text-center">
              <Loader />
            </div>
          ) : (
            <Container>
              <Row className="d-flex justify-content-between">
                <Col sm="12" md="12" xl="7" className="itinerary-big-container">
                  <section className="itinerary-section">
                    <h3 className="text-uppercase sub-heading">Description</h3>
                    <div
                      dangerouslySetInnerHTML={{
                        __html: this.props.itineraryDetails.description
                      }}
                    />
                  </section>
                  <section className="itinerary-section">
                    <h3 className="text-uppercase sub-heading">
                      itinerary gallery
                    </h3>
                    <Row>
                      {this.props.itineraryDetails.gallery.map(
                        (image, index) => (
                          <Col
                            sm="3"
                            key={image}
                            onClick={() =>
                              this.openLIghtBox(`${baseUrl}${image}`)
                            }
                          >
                            <div className="itinerary-gallery-thumb img-object-fit">
                              <a href="javascript:void(0)">
                                <img src={`${baseUrl}${image}`} alt="" />
                              </a>
                            </div>
                          </Col>
                        )
                      )}
                    </Row>
                  </section>
                  <section className="itinerary-section">
                    {/* <h3 className="text-uppercase sub-heading">itinerary</h3>
                    <p>
                      Lorem Ipsum is simply dummy text of the printing and
                      typesetting industry. Lorem Ipsum has been the industry's
                      standard dummy text ever since the 1500s, when an unknown
                      printer took a galley of type and scrambled it to make a
                      type specimen book. It has survived not only five
                      centuries, but also the leap into electronic typesetting,
                      remaining essentially unchanged. It was popularised in the
                      1960s with the release of Letraset sheets containing Lorem
                      Ipsum passages, and more recently with desktop publishing
                      software like Aldus PageMaker including versions of Lorem
                      Ipsum.
                    </p> */}
                    <ul className="itinerary-days-plan">
                      {this.props.itineraryDetails.itineraryDays.map(
                        (item, index) => (
                          <ItineraryDayPlan
                            item={item}
                            baseUrl={baseUrl}
                            index={index}
                            key={index}
                          />
                        )
                      )}
                    </ul>
                  </section>
                  <section className="itinerary-section">
                    <ItineraryDetailsTabs
                      tripDetail={this.props.itineraryDetails.tripDetail}
                    />
                  </section>
                  <div className="review-container">
                    <h3 className="text-uppercase sub-heading">Reviews</h3>
                    {this.props.itineraryDetails.reviews.length != 0 ? (
                      this.props.itineraryDetails.reviews.map((item, index) => {
                        if (item.status === true) {
                          return <ReviewListing key={index} review={item} />;
                        }
                      })
                    ) : (
                      <strong>No Reviw Posted</strong>
                    )}
                  </div>
                  <div className="review-form">
                    <h3 className="text-uppercase sub-heading">
                      leave your review / Query
                    </h3>
                    <ReviewForm
                      onSubmitReview={this.onSubmitReview}
                      reviewStatus={this.props.itineraryReviewStatus}
                    />
                  </div>
                </Col>
                <Col sm="12" md="12" lg="12" xl="4">
                  <div id="itinerary-aside">
                    <div className="itinerary-booking-form">
                      <AdvancedBookingForm
                        submitQueryForm={this.submitQueryForm}
                      />
                    </div>
                    <SocialShareIcons />
                  </div>
                </Col>
              </Row>
            </Container>
          )}
        </section>
        {this.state.isOpen === true ? (
          <Lightbox
            mainSrc={this.state.imageUrl}
            onCloseRequest={() => this.setState({ isOpen: false })}
          />
        ) : null}
        <ToastContainer
          position="bottom-center"
          autoClose={2000}
          hideProgressBar
          newestOnTop
          closeOnClick
          rtl={false}
          pauseOnVisibilityChange
          draggable
          pauseOnHover
        />
      </div>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return {
    fetchItinenaryDetails: payload => {
      dispatch(fetchItinenaryDetails(payload));
    },
    submitItineraryReview: payload => {
      dispatch(submitItineraryReview(payload));
    },
    submitQueryForm: payload => {
      dispatch(submitQueryForm(payload));
    }
  };
}

function mapStateToProps(state) {
  return {
    loadingItineraryDetails: state.ItineraryDetails.loadingItineraryDetails,
    itineraryDetails: state.ItineraryDetails.itineraryDetails,
    itineraryReviewStatus: state.ItineraryDetails.itineraryReviewStatus
  };
}

export default {
  component: connect(
    mapStateToProps,
    mapDispatchToProps
  )(ItineraryDetails),
  loadData: (store, match) =>
    store.dispatch(fetchItinenaryDetails(match.params.slug))
};
